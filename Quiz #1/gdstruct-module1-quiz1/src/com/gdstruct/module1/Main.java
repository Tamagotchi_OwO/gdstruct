package com.gdstruct.module1;

import javax.swing.text.html.HTMLEditorKit;

public class Main {

    public static void main(String[] args) {
	int [] numbers = new int[16];
	    numbers[0] = 79;
	    numbers[1] = -25;
        numbers[2] = -235;
        numbers[3] = 615;
        numbers[4] = 19;
        numbers[5] = 126;
        numbers[6] = 856;
        numbers[7] = -1235;
        numbers[8] = 100;
        numbers[9] = 41;
        numbers[10] = 14;
        numbers[11] = -9;
        numbers[12] = 66;
        numbers[13] = 71;
        numbers[14] = 89734;
        numbers[15] = 523;

        System.out.println("Before Modified Selection Sort:");
        printArrays(numbers);

        System.out.println("\nAfter Modified Selection Sort:");
        selectionSortModified(numbers);
        printArrays(numbers);
    }

    public static void bubbleSortDescending(int [] arr)
    {
        for (int lastSortedIndex = arr.length - 1;lastSortedIndex >= 0; lastSortedIndex--)
        {
            for (int i = 0; i < lastSortedIndex; i++)
            {
                if (arr[i] < arr[i + 1])
                {
                    int temp = arr[i];
                    arr[i]= arr[i + 1];
                    arr[i + 1] = temp;
                }
            }
        }
    }
    public static void selectionSortDescending(int [] arr)
    {
        for (int lastSortedIndex = arr.length - 1; lastSortedIndex > 0; lastSortedIndex--)
        {
            int smallestIndex = 0;

            for (int i = 0; i <= lastSortedIndex; i++)
            {
                if (arr[i] <= arr[smallestIndex])
                {
                    smallestIndex = i;
                }
            }
            int temp = arr[lastSortedIndex];
            arr[lastSortedIndex] = arr[smallestIndex];
            arr[smallestIndex] = temp;
        }
    }

    public static void selectionSortModified(int [] arr)
    {
        int lastSortedIndex;
        int smallestIndex = 0;
        for (lastSortedIndex = arr.length - 1; lastSortedIndex > 0; lastSortedIndex--)
        {
            for (int i = 0; i <= lastSortedIndex; i++)
            {
                if (arr[i] <= arr[smallestIndex])
                {
                    smallestIndex = i;
                }
            }
        }
        int temp = arr[lastSortedIndex];
        arr[lastSortedIndex] = arr[smallestIndex];
        arr[smallestIndex] = temp;
    }

    public static void printArrays(int[] arr)
    {
        for (int j : arr)
        {
            System.out.print(j + "  ");
        }
    }
}

